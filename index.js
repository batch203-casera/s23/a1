let trainer = {
	name: "Ash Ketchum",
	age: 10, 
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"], 
	},
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
};

console.log(trainer);

// Access object properties using dot notation
console.log("Results of the dot notation:");
console.log(trainer.name);

// Access object properties using brackets
console.log("Result of the bracket notation:");
console.log(trainer['pokemon']);

// Invoke the talk object method
console.log("Result of the talk notation:");
trainer.talk();
